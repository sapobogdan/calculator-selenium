package com.bogdan.calculator;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {

    @GetMapping("aduna")
    public ModelAndView add(@RequestParam("nr2") int b, @RequestParam("nr1") Integer a) {
        int result = a + b;
        ModelAndView modelAndView = new ModelAndView("result_add.html");
        modelAndView.addObject("rez", result);
        return modelAndView;
    }

}
